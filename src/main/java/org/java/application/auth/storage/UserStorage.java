package org.java.application.auth.storage;

import org.java.application.auth.User;

import java.util.Optional;
import java.util.stream.Stream;

public interface UserStorage {
    Stream<User> getAll();
    Optional<User> findByLogin(String login);
    void save(User user);
}

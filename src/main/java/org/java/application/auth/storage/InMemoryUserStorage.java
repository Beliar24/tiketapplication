package org.java.application.auth.storage;

import org.java.application.auth.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class InMemoryUserStorage implements UserStorage {
    List<User> userList = new ArrayList<>();


    @Override
    public Stream<User> getAll() {
        return userList.stream();
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return userList.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst();
    }

    @Override
    public void save(User user) {
        userList.add(user);
    }
}

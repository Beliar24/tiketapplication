package org.java.application.auth;

import lombok.RequiredArgsConstructor;
import org.java.application.infrastructure.menu.ui.ViewHelper;

@RequiredArgsConstructor
public class UserView {
    private final ViewHelper viewHelper;

    public User readUser() {
        String login = viewHelper.readString("Enter your login: ");
        String password = viewHelper.readString("Enter your password: ");
        return new User(login, password);
    }

    public void showError(String error) {
        viewHelper.showError(error);
    }
}

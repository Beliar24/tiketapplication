package org.java.application.auth;

import lombok.RequiredArgsConstructor;
import org.java.application.auth.storage.UserStorage;

import java.util.Optional;

@RequiredArgsConstructor
public class AuthService {
    private final UserStorage userStorage;
    private User currentUser = null;

    public void login(User user) {
        currentUser = userStorage.findByLogin(user.getLogin())
                .filter(u -> u.hasPassword(user.getPassword()))
                .orElseThrow(() -> new IllegalArgumentException("Username or password is incorrect"));
    }

    public void register(User user) {
        if (isNullOrEmpty(user.getLogin()) || isNullOrEmpty(user.getPassword())) {
            throw new IllegalArgumentException("Login or password not much rule");
        }

        boolean exists = userStorage.findByLogin(user.getLogin()).isPresent();
        if (exists) {
            throw new IllegalArgumentException("User already exist");
        }

        userStorage.save(user);
    }

    public String getUsername() {
        return currentUser.getLogin();
    }

    public void exit() {
        currentUser = null;
    }

    private boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    public boolean isAuth() {
        return currentUser != null;
    }

    public Optional<User> current() {
        return Optional.ofNullable(currentUser);
    }
}

package org.java.application.auth.menu;

import lombok.RequiredArgsConstructor;
import org.java.application.auth.AuthService;
import org.java.application.infrastructure.menu.MenuItem;

@RequiredArgsConstructor
public class LogoutMenuItem implements MenuItem {

    private final AuthService authService;

    @Override
    public String getName() {
        return "Logout";
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }

    @Override
    public void run() {
        authService.exit();
    }
}

package org.java.application.auth;

import lombok.*;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String login;
    private String password;

    public boolean hasPassword(String password) {
        return Objects.equals(this.password, password);
    }
}

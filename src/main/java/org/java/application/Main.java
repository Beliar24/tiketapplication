package org.java.application;

import org.java.application.auth.AuthService;
import org.java.application.auth.UserView;
import org.java.application.auth.menu.LoginMenuItem;
import org.java.application.auth.menu.LogoutMenuItem;
import org.java.application.auth.menu.RegisterMenuItem;
import org.java.application.ticket.storage.TextFileTicketStorage;
import org.java.application.auth.storage.TextFileUserStorage;
import org.java.application.ticket.storage.TicketStorage;
import org.java.application.infrastructure.menu.ExitMenuItem;
import org.java.application.infrastructure.menu.Menu;
import org.java.application.infrastructure.menu.MenuItem;
import org.java.application.infrastructure.menu.ui.ViewHelper;
import org.java.application.ticket.TicketService;
import org.java.application.ticket.TicketView;
import org.java.application.ticket.menu.ChangeStatusMenuItem;
import org.java.application.ticket.menu.CreateTicketMenuItem;
import org.java.application.ticket.menu.FindTicketMenuItem;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ViewHelper viewHelper = new ViewHelper(scanner);


        TicketStorage storage = new TextFileTicketStorage("tickets.txt");
        TicketView ticketView = new TicketView(viewHelper);


        UserView userView = new UserView(viewHelper);
        TextFileUserStorage textFileUserStorage = new TextFileUserStorage("users.txt");
        AuthService service = new AuthService(textFileUserStorage);
        TicketService ticketService = new TicketService(storage, service);


        List<MenuItem> menuItemList = List.of(
                new LoginMenuItem(service, userView),
                new RegisterMenuItem(service, userView),
                new CreateTicketMenuItem(service, ticketService, ticketView),
                new ChangeStatusMenuItem(service, ticketService, ticketView),
                new FindTicketMenuItem(service, ticketService, ticketView),
                new LogoutMenuItem(service),
                new ExitMenuItem()
        );

        Menu menu = new Menu(menuItemList, scanner);
        menu.run();


    }
}


/*
 *  Создать заявку в которой отображена почта того кто оставил ее, описание проблемы и комментарий +
 * Состояние заявки новая, выполненая , в процессе +
 * в заявке есть кто ее создал и кто ее решает или решил +
 *  Попробовать создать ключ для заявки ( номер или айди по которой ее можно отметить как выполненую +
 * Создать фильтрацию по заявкам, мои заявки, все заявки , заявки по номеру +
 *
 *
 * */

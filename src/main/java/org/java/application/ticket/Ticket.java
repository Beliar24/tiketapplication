package org.java.application.ticket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Ticket {
    private String creator;
    private String assigned;
    private String description;
    private String comment;
    private String status;
    private String id;
}

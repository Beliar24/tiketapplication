package org.java.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.java.application.auth.AuthService;
import org.java.application.infrastructure.menu.MenuItem;
import org.java.application.ticket.TicketService;
import org.java.application.ticket.TicketView;

import java.util.List;

@RequiredArgsConstructor
public class ChangeStatusMenuItem implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;
    private final TicketView ticketView;

    @Override
    public String getName() {
        return "Change status of ticket";
    }

    @Override
    public void run() {
        List<String> list = ticketView.selectStatus();
        try {
            ticketService.changeTicketStatus(list.get(0), list.get(1));
        } catch (IllegalArgumentException e) {
            ticketView.showError(e.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}

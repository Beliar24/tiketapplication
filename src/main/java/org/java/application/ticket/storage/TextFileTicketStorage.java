package org.java.application.ticket.storage;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.java.application.ticket.Ticket;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class TextFileTicketStorage implements TicketStorage {

    private final String filePath;

    @Override
    public Stream<Ticket> getAll() {
        try {
            return Files.lines(Path.of(filePath))
                    .filter(line -> !line.trim().isEmpty())
                    .filter(line -> !line.trim().equals("#"))
                    .map(this::extractTicket);
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    @Override
    public Optional<Ticket> findByNumber(String id) {
        return getAll()
                .filter(t -> t.getId().equals(id))
                .findFirst();
    }

    private Ticket extractTicket(String s) {
        String[] parts = s.split("\\s\\|\\s");
        return new Ticket(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5]);
    }

    @SneakyThrows
    @Override
    public void changeStatus(String id, String status) {
        int counter = 0;
        List<String> lines = Files.readAllLines(Paths.get(filePath));
        for (String s : lines) {
            if (s.contains(id)) {
                if (s.contains("New")) {
                    String s1 = lines.get(counter).replace("New", status);
                    lines.add(s1);
                    lines.remove(counter);
                    break;
                } else if (s.contains("In progress")) {
                    String s1 = lines.get(counter).replace("In progress", status);
                    lines.add(s1);
                    lines.remove(counter);
                    break;
                } else if (s.contains("Success")) {
                    System.out.println("Cannot change status, the ticket closed");
                    break;
                } else {
                    throw new IllegalArgumentException("Incorrect status");
                }
            } else {
                throw new IllegalArgumentException("Incorrect id");
            }
        }
        Files.write(Paths.get(filePath), lines);
    }

    @Override
    public void save(Ticket ticket) {
        try {
            Files.write(Path.of(filePath), toLine(ticket), StandardOpenOption.WRITE, StandardOpenOption.CREATE,
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Iterable<? extends CharSequence> toLine(Ticket ticket) {
        return List.of(ticket.getCreator() + " | " + ticket.getAssigned() + " | " + ticket.getDescription() + " | " +
                ticket.getComment() + " | " + ticket.getStatus() + " | " + ticket.getId());
    }

}

package org.java.application.ticket.storage;

import org.java.application.ticket.Ticket;

import java.util.Optional;
import java.util.stream.Stream;

public interface TicketStorage {
    Stream<Ticket> getAll();
    Optional<Ticket> findByNumber(String number);
    void changeStatus(String id, String status);
    void save(Ticket ticket);
}

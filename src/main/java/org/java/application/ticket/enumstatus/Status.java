package org.java.application.ticket.enumstatus;

public enum Status {
    NEW("New"),
    INPROGRESS("In progress"),
    SUCCESS("Success");

    private final String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}



package org.java.application.infrastructure.menu;

import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class Menu {
    private final List<MenuItem> itemList;
    private final Scanner scanner;

    public void run() {
        while (true) {
            show();
            Optional<MenuItem> item = getChosen();
            if (item.isPresent()) {
                item.get().run();
                if (item.get().isFinal()) break;
            } else {
                System.out.println("Incorrect chose, try again");
            }
        }
    }

    private Optional<MenuItem> getChosen() {
        int chosen = readChosen();
        List<MenuItem> visibleItem = getVisible();
        if (chosen < 0 || chosen >= visibleItem.size()) {
            return Optional.empty();
        }
        return Optional.of(visibleItem.get(chosen));
    }

    private int readChosen() {
        System.out.println("Enter your chose: ");
        int chose = scanner.nextInt();
        scanner.nextLine();
        return chose - 1;
    }

    private void show() {
        List<MenuItem> visibleMenuItem = getVisible();
        for (int i = 0 ; i < visibleMenuItem.size(); i++) {
            System.out.printf("%d - %s\n", i + 1, visibleMenuItem.get(i).getName());
        }
    }

    private List<MenuItem> getVisible() {
        return itemList.stream().filter(MenuItem::isVisible).collect(Collectors.toList());
    }
}

package org.java.application.infrastructure.menu.ui;

import lombok.RequiredArgsConstructor;

import java.util.Scanner;

@RequiredArgsConstructor
public class ViewHelper {

    private final Scanner scanner;

    public String readString(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    public int readInt(String message) {
        System.out.println(message);
        int val = scanner.nextInt();
        scanner.nextLine();
        return val;
    }

    public void showError(String error) {
        System.out.println(error);
    }

}

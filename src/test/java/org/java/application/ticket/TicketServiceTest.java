package org.java.application.ticket;

import org.java.application.ticket.storage.TicketStorage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TicketServiceTest {

    private static final String CREATOR = "Den@gmail.com";
    private static final String ASSIGNED = "Vasya";
    private static final String DESCRIPTION = "This is description";
    private static final String COMMENT = "+++";
    private static final String STATUS = "In progress";
    private static final String ID = "14";
    private static final Ticket TICKET = new Ticket(CREATOR, ASSIGNED, DESCRIPTION, COMMENT, STATUS, ID);

    @InjectMocks
    TicketService ticketService;
    @Mock
    TicketStorage ticketStorage;

    @Test
    void shouldNotCreateWhenTicketDataIsIncorrect() {
        Ticket ticket = new Ticket(null, null, null, null, null, null);

        assertThrows(IllegalArgumentException.class, () -> ticketService.createTicket(ticket),
                "User login or password not much the rule");

        verify(ticketStorage, never()).save(any());
        verify(ticketStorage, never()).findByNumber(any());
    }

    @Test
    void shouldNotChangeStatusWhenTicketNotExists() {
        ticketService.createTicket(TICKET);

        Ticket ticket = ticketService.getTicket();

        assertEquals("In progress", ticket.getStatus());
    }

    @Test
    void shouldBeCorrectSearchByID() {
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(TICKET);

        when(ticketStorage.getAll()).thenReturn(tickets.stream().distinct());

        assertEquals(ID, ticketService.findTickets(ID).get(0).getId());
    }

    @Test
    void shouldBeCorrectSearchByStatus() {
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(TICKET);

        when(ticketStorage.getAll()).thenReturn(tickets.stream().distinct());

        assertEquals(STATUS, ticketService.findTickets(ID).get(0).getStatus());
    }
}